const express = require('express');
const fileDb = require('../fileDb');
const router = express.Router();


router.get('/', (req, res) => {
    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/', (req, res) => {

    if (!req.body.message) {
        return res.status(400).send({error: 'Message not valid'});
    }

    const newMessage = fileDb.addItem({
        message: req.body.message,
        dateTime: new Date().toISOString(),
    });
    res.send(newMessage);
});

module.exports = router;
