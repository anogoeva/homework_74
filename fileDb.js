const fs = require('fs');
let data = [];
let lastData = [];
let message = {};
let dateTime = {};
let messageItem = {};
let returnData = [];
const path = "./messages/";

module.exports = {
    init() {
        try {
            const fileContents = fs.readdirSync(path);
            data.push(fileContents);
        } catch (e) {
            data = [];
        }
    },

    getItems() {

        fs.readdirSync(path).forEach(files => {
            data.push(files);
            lastData = data.slice(Math.max(data.length - 5, 0));
        });
        lastData.forEach(pathFileName => {
            try {
                const data = fs.readFileSync(path + pathFileName, 'utf8');
                const obj = JSON.parse(data);
                returnData.push(obj);
            } catch (err) {
                console.error(err);
            }
        })
        return returnData && returnData.splice(0, returnData.length);
    },

    addItem(item) {
        messageItem = item;
        message = item.message;
        dateTime = item.dateTime;
        this.save();
        return item;
    },

    save() {
        fs.writeFileSync("messages/" + dateTime + ".txt", JSON.stringify(messageItem));
    }
};