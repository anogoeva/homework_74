const express = require('express');
const messages = require('./app/messages');
const fileDb = require('./fileDb');


const app = express();
app.use(express.json());
const port = 8000;

app.use('/messages', messages);

fileDb.init();
app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});